module SwitchState
  class Wall
    attr_accessor :switches

    def initialize(count)
      @switches = []
      count.to_i.times do
        @switches << Switch.new
      end
    end

    def toggle_switch_range from, to
      a, z = from, to
      if from > to
        z, a = from, to
      end

      (a..z).each do |index|
        switches[index.to_i].toggle
      end
    end
  end
end
