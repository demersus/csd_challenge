module SwitchState
  class Switch
    attr_accessor :on
    alias on? on

    def initialize(on: false)
      @on = on
    end

    def toggle
      @on = !@on
    end
  end
end
