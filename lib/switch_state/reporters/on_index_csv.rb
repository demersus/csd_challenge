module SwitchState
  module Reporters
    class OnIndexCsv
      def initialize switches
        @switches = switches
      end

      def to_s
        list = []
        @switches.each_with_index do |switch, index|
          list << index if switch.on?
        end
        list.join ', '
      end
    end
  end
end
