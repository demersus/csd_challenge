module SwitchState
  module Reporters
    class TotalOn
      def initialize(switches)
        @switches = switches
      end

      def to_s
        @switches.count(&:on?).to_s
      end
    end
  end
end
