require 'active_support/inflector'

class MakingChange::MinimumCoins
  COIN_VALUES = {
    quarter: 0.25,
    dime: 0.10,
    nickel: 0.05,
    penny: 0.01
  }

  def self.call change
    self.new(change).call
  end

  def initialize change
    @change = change
  end

  def call
    coins_to_sentence
  end

  def coins
    coin_counts = {}

    # Convert to integers to avoid float division which is not accurate enough
    # for these calculations.
    COIN_VALUES.inject(@change.dup * 100) do |remainder, (coin, value)|
      int_value = (value * 100).to_i
      if remainder > value
        count = (remainder / int_value).floor

        coin_counts[coin] = count

        # New Reminder
        remainder -  count * int_value
      else
        remainder
      end
    end

    coin_counts
  end

  private
  def coins_to_sentence
    coins.map do |(coin, count)|
      if count > 0
        "#{count} #{coin.to_s.capitalize.pluralize(count)}"
      end
    end.compact.join(', ')
  end

end
