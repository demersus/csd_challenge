class WordWrap::Wrapper
  def self.call *args
    new(*args).call
  end

  def initialize(text, length: 40)
    @text = text
    @length = length
  end

  def call
    recursively_extract_lines(@text.dup).join("\n")
  end

  private

  def recursively_extract_lines(text, lines = [])
    line = find_next_line text
    lines << line

    remainder = remove_line_and_leading_whitespace text, line

    if remainder.length > @length
      return recursively_extract_lines(remainder, lines)
    elsif remainder.length > 0
      lines << remainder
    end

    return lines
  end

  def find_next_line(text)
    line = text[0..@length]
    if line.length <= @length
      return line
    else
      shorten_to_last_boundary(line)
    end
  end

  def shorten_to_last_boundary(text)
    text.rpartition(/\s+/).first
  end

  def remove_line_and_leading_whitespace(text, line)
    text.slice(line.length, text.length).lstrip
  end
end
