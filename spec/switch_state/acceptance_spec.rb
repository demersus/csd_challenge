require 'spec_helper'
require 'switch_state'

RSpec.describe "Switch State" do
  def report_on_count(wall)
    SwitchState::Reporters::TotalOn.new(wall.switches).to_s
  end

  describe "Example Scenario starting with 10 switches" do
    it do
      wall = SwitchState::Wall.new 10

      wall.toggle_switch_range 3, 6
      wall.toggle_switch_range 0, 4
      wall.toggle_switch_range 7, 3
      wall.toggle_switch_range 9, 9

      expect(report_on_count(wall)).to eql '7'
    end
  end

  describe "Challenge scenario" do
    it do
      wall = SwitchState::Wall.new 1000

      wall.toggle_switch_range 616, 293
      wall.toggle_switch_range 344, 942
      wall.toggle_switch_range 27 , 524
      wall.toggle_switch_range 716, 291
      wall.toggle_switch_range 860, 284
      wall.toggle_switch_range 74 , 928
      wall.toggle_switch_range 970, 594
      wall.toggle_switch_range 832, 772
      wall.toggle_switch_range 343, 301
      wall.toggle_switch_range 194, 882
      wall.toggle_switch_range 948, 912
      wall.toggle_switch_range 533, 654
      wall.toggle_switch_range 242, 792
      wall.toggle_switch_range 408, 34
      wall.toggle_switch_range 162, 249

      expect(report_on_count(wall)).to eql '494'
    end
  end
end
