require 'spec_helper'
require 'making_change'

RSpec.describe MakingChange::MinimumCoins do
  let(:change) {}
  subject { described_class.call(change) }

  scenarios = {
    0.56 => "2 Quarters, 1 Nickel, 1 Penny",
    0.87 => "3 Quarters, 1 Dime, 2 Pennies",
    0.98 => "3 Quarters, 2 Dimes, 3 Pennies",
    1.41 => "5 Quarters, 1 Dime, 1 Nickel, 1 Penny",
    0.39 => "1 Quarter, 1 Dime, 4 Pennies",
    1.66 => "6 Quarters, 1 Dime, 1 Nickel, 1 Penny"
  }

  scenarios.each do |(input, output)|
    context "Given #{input}," do
      let(:change) { input }

      it do
        expect(subject).to eql output
      end
    end
  end
end
