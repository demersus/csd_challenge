require 'spec_helper'
require 'word_wrap'

RSpec.describe WordWrap::Wrapper do
  subject { described_class.call input }

  context 'default wrap of 40 chars, main scenario' do
    let(:input) {
<<-EOS
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus bibendum ectus aliquam volutpat ultricies. Ut quis augue quis mauris semper varius et ut faucibus orci luctus et ultrices posuere cubilia Curae; Praesent porttitor tristique faucibus. Curabitur commodo augue vitae lacus viverra maximus. Vestibulum ante "ipsum primis" in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur eleifend at lacus ac mollis.
EOS
    }

    let(:word_wrapped) {
<<-EOS
Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Vivamus bibendum ectus
aliquam volutpat ultricies. Ut quis
augue quis mauris semper varius et ut
faucibus orci luctus et ultrices posuere
cubilia Curae; Praesent porttitor
tristique faucibus. Curabitur commodo
augue vitae lacus viverra maximus.
Vestibulum ante "ipsum primis" in
faucibus orci luctus et ultrices posuere
cubilia Curae; Curabitur eleifend at
lacus ac mollis.
EOS
    }

    it do
      expect(subject).to eql word_wrapped
    end
  end

  describe "only 40 char line" do
    let(:input) {
      "Lorem ipsum dolor sit amet, consecteturs"
    }

    let(:wrapped) {
      "Lorem ipsum dolor sit amet, consecteturs"
    }

    it do
      expect(subject).to eql wrapped
    end
  end

  describe 'sentence which will wrap to 2 lines' do
    let(:input) {
      'Vestibulum ante "ipsum primis" in faucibus orci luctus et ultrices posuere'
    }

    let(:wrapped) {
'Vestibulum ante "ipsum primis" in
faucibus orci luctus et ultrices posuere'
    }

    it do
      expect(subject).to eql wrapped
    end

  end

  describe 'sentence which will wrap to 3 lines; last line is not full length' do
    let(:input) {
<<-EOS
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus bibendum ectus aliquam volutpat ultricies.
EOS
    }

    let(:wrapped) {
<<-EOS
Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Vivamus bibendum ectus
aliquam volutpat ultricies.
EOS
    }

    it do
      expect(subject).to eql wrapped
    end

  end

  describe 'sentence with 80 chars (two equal lines)' do
    let(:input) {
<<-EOS
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus bibendum ectus.
EOS
    }

    let(:wrapped) {
"Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Vivamus bibendum ectus."
    }

    it do
      expect(subject).to eql wrapped
    end

  end
end
